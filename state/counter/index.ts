/**
 * Import everything as name variables
 * so that we can export them as part
 * of the default object.
 * This is used for imports outside
 * of this folder
 */
import * as types from './counter.types';
import * as actions from './counter.actions';
import * as state from './counter.state';
import * as reducer from './counter.reducer';

/**
 * Create default object so that we can
 * export default later
 */


const counter = {
  types,
  actions,
  state,
  reducer,
};

/**
 * Export bulk exports from each file.
 * This is used for imports inside of this folder
 */
export * from './counter.types';
export * from './counter.actions';
export * from './counter.state';
export * from './counter.reducer';

/**
 * Export the default variable.
 * This is used for imports outside of this folder
 */
export default counter;
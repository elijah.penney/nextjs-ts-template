import { CounterActionTypes, INCREMENT_COUNTER, DECREMENT_COUNTER } from '.';

export function incrementCounter(currentCounter: number): CounterActionTypes {
  return {
    type: INCREMENT_COUNTER,
    payload: currentCounter + 1,
  };
}

export function decrementCounter(currentCounter: number): CounterActionTypes {
  return {
    type: DECREMENT_COUNTER,
    payload: currentCounter - 1,
  };
}
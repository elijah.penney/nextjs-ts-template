import { state as initState, CounterActionTypes, CounterState } from '.';

export function reducer(state = initState, action: CounterActionTypes): CounterState {
  const { type, payload } = action;

  switch (type) {
    case 'DECREMENT_COUNTER':
      return {
        count: payload,
      };
    case 'INCREMENT_COUNTER':
      return {
        count: payload,
      };
    default:
      return state;
  }
}
import { combineReducers } from 'redux';
import { reducer as counter } from '@/counter';

const reducer = combineReducers({
  counter,
});

export {
  reducer,
};
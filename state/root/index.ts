import * as reducer from './root.reducer';

const root = {
  reducer,
};

export * from './root.reducer';
export default root;
import { NextPage } from 'next';
import styled from 'styled-components';
import { connect } from 'react-redux';
import { Counter } from '~';
import { incrementCounter, decrementCounter, CounterState } from '@/counter';

const Content = styled.div`
  height: 100vh;
  width: 100vw;
  display: flex;
  justify-content: space-around;
  align-items: center;
`;

interface Props {
  counter: CounterState,
  increment: () => void,
  decrement: () => void,
}

const Home: NextPage<Props> = (props) => {
  const { counter: { count }, increment, decrement } = props;

  return (
    <Content>
      <Counter
        increment={increment}
        decrement={decrement}
        current={count}
      />
    </Content>
  );
};

const mapStateToProps = ({ counter }) => ({
  counter,
});

const mapDispatchToProps = dispatch => ({
  increment: (count) => dispatch(incrementCounter(count)),
  decrement: (count) => dispatch(decrementCounter(count)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Home);

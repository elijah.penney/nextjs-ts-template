import React from 'react';
import { Button } from 'antd';
import styled from 'styled-components';
import PropTypes from 'prop-types';

const Container = styled.div`
  width: 250px;
  height: 250px;
  display: flex;
  justify-content: space-around;
  align-content: center;
  flex-direction: column;
`;

const NumberDisplay = styled.div`
  font-size: 48px;
  text-align: center;
  width: 100%;
`;

const Controls = styled.div`
  display: flex;
  justify-content: space-around;
  align-items: center;
`;

interface Props {
  /** Function to increment the counter */
  increment: (number) => void,

  /** Function to decrement the counter */
  decrement: (number) => void,

  /** The current counter value */
  current: number
}

const Counter: React.FC<Props> = props => {
  const { increment, decrement, current } = props;

  return (
    <Container>
      <NumberDisplay>
        {current}
      </NumberDisplay>
      <Controls>
        <Button onClick={() => increment(current)}>Increment</Button>
        <Button onClick={() => decrement(current)}>Decrement</Button>
      </Controls>
    </Container>
  );
};

Counter.propTypes = {
  current: PropTypes.number.isRequired,
  increment: PropTypes.func.isRequired,
  decrement: PropTypes.func.isRequired,
};

export { Counter };